using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCtrl : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform firePos;
    public AudioClip fireSfx;
    private RaycastHit hit;

    [SerializeField]
    private new MeshRenderer renderer;
    private new AudioSource audio;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        // MuzzleFlash에 있는 MeshRenderer 컴포넌트를 할당
        renderer = firePos.GetComponentInChildren<MeshRenderer>();
        renderer.enabled = false;
    }

    // 매 프레임 마다 호출되는 메소드(함수)
    void Update()
    {
        if (Input.GetMouseButtonDown(0) == true)
        {
            // 총알 투사체 발사
            Fire();

            // 광선의 시각화
            Debug.DrawRay(firePos.position, firePos.transform.forward * 20.0f, Color.green, 0.5f);
            // 레이캐스트 (광선에 닿은 물체는 hit에 정보가 담김)
            if (Physics.Raycast(firePos.position, firePos.transform.forward, out hit, 20.0f, 1 << 8))   // 2^8 == 256
            {
                // 대미지를 전달 (레이어를 통해 감지 범위를 줄일 수 있음)
                hit.collider.GetComponent<MonsterCtrl>().OnDamage(25.0f);
            }
            
        }
    }

    void Fire()
    {
        // 총알 생성
        // Instantiate(생성할객체, 위치, 회전각도)
        Instantiate(bulletPrefab, firePos.position, firePos.rotation);
        // 총소리 발생
        audio.PlayOneShot(fireSfx, 0.8f);
        // 총구화염 효과
        StartCoroutine(ShowMuzzleFlash());
    }

    IEnumerator ShowMuzzleFlash()
    {
        // 오프셋 값 변경
        Vector2 offset = new Vector2(Random.Range(0,2), Random.Range(0,2)) * 0.5f;
        renderer.material.mainTextureOffset = offset;

        // 회전처리
        float angle = Random.Range(0.0f, 360.0f);
        renderer.transform.localRotation = Quaternion.Euler(Vector3.forward * angle);

        // 스케일 변경
        float scale = Random.Range(1.0f, 3.0f);
        renderer.transform.localScale = Vector3.one * scale;

        // MuzzleFlash 활성화
        renderer.enabled = true;

        // 잠시 기다리는 로직
        yield return new WaitForSeconds(0.2f);
        
        // MuzzleFlash 비활성화
        renderer.enabled = false;
    }
}
