using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // 싱글턴***
    public static GameManager instance = null;

    public Transform[] points;
    public GameObject monsterPrefab;
    public float createTime = 3.0f;
    public bool isGameOver = false;

    void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        points = GameObject.Find("SpawnPointGroup")?.GetComponentsInChildren<Transform>();
        // 위 코드를 확장한 모습
        /* GameObject obj = GameObject.Find("SpawnPointGroup");
        if (obj != null)
        {
            points = obj.GetComponentsInChildren<Transform>();
        } */

        // Resources 폴더에서 몬스터 프리팹 로드(형 변환 필수)
        //monsterPrefab = Resources.Load("Monster") as GameObject;

        // Monster라는 이름의 에셋을 게임오브젝트로 로드(위 코드와 동일)
        monsterPrefab = Resources.Load<GameObject>("Prefabs/Monster");

        // 반복 호출
        //InvokeRepeating("CreateMonster", 2.0f, createTime);
        StartCoroutine(CreateMonster());
    }

    IEnumerator CreateMonster()
    {
        // 시작하고 2초 뒤 반복
        yield return new WaitForSeconds(2.0f);

        // isGameOver가 true가 될 때 까지 createTime(3초) 만큼 몬스터 반복 생성
        while(!isGameOver)
        {
            // 불규칙한 위치 선정(난수 발생)
            int idx = Random.Range(1, points.Length);   // Points의 개수
            // 몬스터 생성(Instantiate)
            Instantiate(monsterPrefab, points[idx].position, Quaternion.identity);
            yield return new WaitForSeconds(createTime);
        }
    }
}
