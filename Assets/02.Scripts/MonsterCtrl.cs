using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterCtrl : MonoBehaviour
{
    [System.NonSerialized]          // C# (public 속성을 유지하며 인스펙터창에서 나타내지 않음)
    public Transform monsterTr;
    [HideInInspector]               // UnityEngine
    public Transform playerTr;


    // 몬스터의 상태 열거형
    public enum State
    {
        IDLE, TRACE, ATTACK, DIE
    }

    // 몬스터의 상태
    public State state = State.IDLE;

    // 추적 사정거리
    [Range(10.0f, 50.0f)]
    public float traceDist = 50.0f;

    // 공격 사정거리
    public float attackDist = 2.0f;

    private NavMeshAgent agent;
    private Animator anim;

    // 몬스터의 사망 여부 변수
    public bool isDie = false;

    // 몬스터의 Hash테이블 값을 추출해 저장할 변수
    private int hashTrace;
    private int hashAttack;
    private int hashHit;
    private int hashDie;

    // 몬스터 체력
    private float hp = 100.0f;

    // Start is called before the first frame update
    void Start()
    {
        hashTrace   = Animator.StringToHash("IsTrace");
        hashAttack  = Animator.StringToHash("IsAttack");
        hashHit     = Animator.StringToHash("Hit");
        hashDie     = Animator.StringToHash("Die");

        monsterTr   = GetComponent<Transform>();
        playerTr    = GameObject.FindGameObjectWithTag("PLAYER").transform;
        agent       = GetComponent<NavMeshAgent>();
        anim        = GetComponent<Animator>();

        StartCoroutine(CheckState());
        StartCoroutine(MonsterAction());
    }

    // 0.3초 간격으로 무한루프를 돌며 몬스터의 상태를 확인하는 코루틴
    IEnumerator CheckState()
    {
        while (isDie == false)
        {
            if (state.Equals(State.DIE)) yield break;

            // 두 3차원 좌표간의 거리를 측정
            float distance = Vector3.Distance(monsterTr.position, playerTr.position);
            
            // 거리에 따른 몬스터의 상태 변경
            if (distance <= attackDist)
            {
                state = State.ATTACK;
            }
            else if (distance <= traceDist)
            {
                state = State.TRACE;
            }
            else
            {
                state = State.IDLE;
            }

            // 0.3초 시간이 흐르는 만큼 양보
            yield return new WaitForSeconds(0.3f);
        }   // 0.3초가 지나면 다시 while문 첫줄로 돌아감
    }

    // 몬스터의 상태에 따른 액션 반영
    IEnumerator MonsterAction()
    {
        while(!isDie)
        {
            switch (state)
            {                
                case State.IDLE:
                    agent.isStopped = true;                     // 네비게이션 정지
                    anim.SetBool(hashTrace, false);             // Walk -> Idle
                    break;

                case State.TRACE:
                    agent.SetDestination(playerTr.position);    // 타겟까지 도달하는 경로를 계산해 지정
                    agent.isStopped = false;
                    anim.SetBool(hashTrace, true);              // Idle -> Walk
                    anim.SetBool(hashAttack, false);            // Attack -> Walk
                    break;

                case State.ATTACK:
                    agent.isStopped = true;
                    anim.SetBool(hashAttack, true);             // Walk -> Attack
                    break;

                case State.DIE:
                    isDie = true;
                    agent.isStopped = true;
                    anim.SetTrigger(hashDie);
                    GetComponent<CapsuleCollider>().enabled = false;
                    break;
            }
            yield return new WaitForSeconds(0.3f);
        }
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.collider.CompareTag("BULLET"))
        {
            Destroy(coll.gameObject); // 총알 삭제
        }
    }

    public void OnDamage(float damage)
    {
        // 몬스터 피격 시 애니메이션
        anim.SetTrigger(hashHit);

        // 피격 시 damage 수치만큼 hp 감소, hp가 0 이하로 떨어지면 DIE 상태로 변경
        hp -= damage;
        if (hp <= 0.0f)
        {
            state = State.DIE;
        }
    }

    void YouWin()
    {
        StopAllCoroutines();
        agent.isStopped = true;
        anim.SetTrigger("PlayerDie");
    }
}