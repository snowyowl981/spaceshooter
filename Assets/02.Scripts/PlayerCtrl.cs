using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerCtrl : MonoBehaviour
{
    public  float moveSpeed = 10.0f;
    public  float turnSpeed = 80.0f;        // 인스펙터로 노출하여 수정이 가능하게끔 설정

    private float _turnSpeed;               // 실제 게임 내부에서의 회전속도
    public  float cameraRotationLimit;      // 카메라 X축 제한
    public  float currentCameraRotationX;   // 현재 카메라 X축 값

    [SerializeField] 
    private Camera theCamera;
    [SerializeField]
    private Animation anim;
    private GameManager gameManager;

    private float initHp = 100.0f;
    public float currHp = 100.0f;

    // 1회 호출되는 함수
    IEnumerator Start()
    {
        // Animation 컴포넌트 추출
        anim = GetComponent<Animation>(); //제너릭 문법
        //anim = GetComponent("Animation") as Animation;
        anim.Play("Idle");

        _turnSpeed = 0;
        // 시작시 들어오는 가비지값을 방지하고 정확한 위치로 조정하기 위함
        yield return new WaitForSeconds(0.2f);
        _turnSpeed = turnSpeed;

        /*
        메소드(함수)
        객체.대문자(인자);

        프로퍼티(속성)
        객체.소문자 = 값;
        */
    }

    // 화면을 렌더링하는 주기
    void Update()
    {
        float h = Input.GetAxis("Horizontal"); // 좌/우 화살표 키 값을 입력 -1.0f ~ 0.0f ~ +1.0f
        float v = Input.GetAxis("Vertical");
        float r = Input.GetAxis("Mouse X");
        // Debug.Log("h=" + h); //콘솔 뷰에 출력
        // Debug.Log("v=" + v);

        // 플레이어 캐릭터 이동방향
        Vector3 moveDir = (Vector3.forward * v) + (Vector3.right * h);

        // 플레이어 캐릭터 이동
        transform.Translate(moveDir.normalized * Time.deltaTime * moveSpeed);

        // 플레이어 캐릭터 Y축회전
        //transform.Rotate(Vector3.up * Time.deltaTime * r * _turnSpeed);
        var targetRotation = theCamera.transform.eulerAngles.y;     // 카메라 Y축 회전값 받아옴
        // 카메라 Y축 회전만큼 플레이어 Y 회전
        transform.eulerAngles = Vector3.up * targetRotation;

        PlayerAnimation(h, v);
        //CameraRotation();
    }

    // Player Animation 처리 로직
    void PlayerAnimation(float h, float v)
    {
        // 전진/후진
        if (v >= 0.1f)
        {
            anim.CrossFade("RunF", 0.3f); // 전진 애니메이션
        }
        else if (v <= -0.1f)
        {
            anim.CrossFade("RunB", 0.3f); // 후진 애니메이션
        }
        else if (h >= 0.1f)
        {
            anim.CrossFade("RunR", 0.3f); // 오른쪽 
        }
        else if (h <= -0.1f)
        {
            anim.CrossFade("RunL", 0.3f); // 왼쪽
        }
        else
        {
            anim.CrossFade("Idle", 0.3f);
        }
    }

    // 카메라 상하 움직임
    private void CameraRotation()
    {
        // 마우스 Y축값 받아오기
        float _xRotation = Input.GetAxis("Mouse Y");
        // 게임 내에서 카메라 X축 회전값을 저장하는 임시 변수
        float _cameraRotationX = _xRotation * turnSpeed * Time.deltaTime;

        // 현재 카메라 회전값 변경
        currentCameraRotationX -= _cameraRotationX;
        // 카메라 X축 회전값 제한
        currentCameraRotationX = Mathf.Clamp(currentCameraRotationX, -cameraRotationLimit, cameraRotationLimit);

        // 카메라 회전
        // theCamera.transform.localEulerAngles = new Vector3(currentCameraRotationX, 0, 0);
        // Debug.Log(vCamera.m_Follow.position);
    }

    void OnTriggerEnter(Collider coll)
    {
        // PlayerDie가 연속으로 호출되지 않게 하기위한 조건
        if (currHp > 0.0f && coll.CompareTag("PUNCH"))
        {
            //Debug.Log(coll.gameObject.name);
            currHp -= 10.0f;
            if (currHp <= 0.0f)
            {
                PlayerDie();
            }
        }
    }

    void PlayerDie()
    {
        Debug.Log("주인공 사망");
        // 게임매니저의 isGameOver true로 변경
        GameManager.instance.isGameOver = true;

        // 게임오브젝트 중 "MONSTER" 태그가 붙은 객체들을 찾아 내부의 YouWin 함수 호출
        GameObject[] monsters = GameObject.FindGameObjectsWithTag("MONSTER");
        foreach (GameObject monster in monsters)
        {
            monster.SendMessage("YouWin", SendMessageOptions.DontRequireReceiver);
        }
    }
}